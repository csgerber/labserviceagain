package edu.uchicago.gerber.labserviceagain;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


     MyService mService;
     boolean mBound = false;

     private EditText edtMessage;
     private Button btnIntentService;

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            MyService.LocalBinder binder = (MyService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
        Intent intent = new Intent(this, MyService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(connection);
        mBound = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtMessage = findViewById(R.id.edtMessage);
        btnIntentService = findViewById(R.id.btnIntentService);
        btnIntentService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent serviceIntent = new Intent(MainActivity.this, MyIntentService.class);
                Bundle bundle = new Bundle();

                bundle.putString("MESSAGE", edtMessage.getText().toString());
                bundle.putInt("TIMES", 100);
                bundle.putLong("MILLISECONDS", 100L);
                serviceIntent.putExtras(bundle);


                MainActivity.this.startService(serviceIntent);
            }
        });
    }
    // Start the service
    public void startService(View view) {
       // startService(new Intent(this, MyService.class));

    }
    // Stop the service
    public void stopService(View view) {
       // stopService(new Intent(this, MyService.class));
        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);
        finish();
    }


}
