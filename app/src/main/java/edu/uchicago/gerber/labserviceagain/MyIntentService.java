package edu.uchicago.gerber.labserviceagain;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

public class MyIntentService extends IntentService {

  /*
//IntentService versus Service
https://stackoverflow.com/questions/15524280/service-vs-intentservice

//yet another explanation of IntentService versus Service
https://www.journaldev.com/20735/android-intentservice-broadcastreceiver

//official docs for Intent service.
https://developer.android.com/reference/android/app/IntentService
 */
  public MyIntentService() {
      super("MyIntentService");
  }

    public MyIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent( @Nullable Intent intent) {

        long milliseconds = intent.getLongExtra("MILLISECONDS", 100);
        String message = intent.getStringExtra("MESSAGE");
        int times = intent.getIntExtra("TIMES", 10);

        for (int nC = 0; nC < times; nC++){
            try {
                Thread.sleep(milliseconds);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.d("YES", message + nC);
        }



    }




}
