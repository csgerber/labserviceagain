package edu.uchicago.gerber.labserviceagain;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.provider.Settings;
import android.widget.Toast;

//https://www.tutlane.com/tutorial/android/android-services-with-examples

public class MyService extends Service {

    private final IBinder binder = new LocalBinder();


    private MediaPlayer player;
    @Override
    public IBinder onBind(Intent intent) {
        return  null;
    }
    @Override
    public void onCreate() {

        player = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI);
        // This will play the ringtone continuously until we stop the service.
        player.setLooping(true);
        // It will start the player
        player.start();
        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();

        //Toast.makeText(this, "Service was Created", Toast.LENGTH_LONG).show();
    }


    //    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        player = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI);
//        // This will play the ringtone continuously until we stop the service.
//        player.setLooping(true);
//        // It will start the player
//        player.start();
//        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
//        //if it's sticky, the OS will try to restart it if it get's killed
//        return START_NOT_STICKY;
//    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stopping the player when service is destroyed
        player.stop();
        Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show();
    }


    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        MyService getService() {
            // Return this instance of LocalService so clients can call public methods
            return MyService.this;
        }
    }
}